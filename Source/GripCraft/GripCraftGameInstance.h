// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"

#include "Entities/Block.h"

#include "Engine/DataTable.h"

#include "GripCraftGameInstance.generated.h"

/**
 * 
 */


UENUM(BlueprintType)
enum EBlockType
{
	Air     UMETA(DisplayName = "Air"),
	Grass     UMETA(DisplayName = "Grass"),
	Gravel      UMETA(DisplayName = "Gravel"),
	Stone   UMETA(DisplayName = "Stone"),
	Wood   UMETA(DisplayName = "Wood"),
	MAX   UMETA(DisplayName = "MAX"),
};

USTRUCT(BlueprintType)
struct FBlockTypeClassRow : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		TEnumAsByte<EBlockType> BlockType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
		TSubclassOf<class ABlock> BlockClass;
};

USTRUCT(BlueprintType)
struct FChunkData
{
	GENERATED_USTRUCT_BODY()


public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
	FIntPoint Location;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
	int Size;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
		FIntVector HeighestBlock;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Default")
		TArray<TEnumAsByte<EBlockType>> Blocks;

	int GetLocationIndex(int chunkSize, int x, int y, int z) {
		return z * chunkSize * chunkSize + y * chunkSize + x;
	}

	int GetLocationIndex(int chunkSize, FIntVector location) {
		return location.Z * chunkSize * chunkSize + location.Y * chunkSize + location.X;
	}
};

UCLASS()
class GRIPCRAFT_API UGripCraftGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	class ABlock* PlaceBlock(FVector Location, TEnumAsByte<EBlockType> BlockClass);

	class ABlock* SpawnBlockGenerated(FIntPoint ChunkLocation, FIntVector BlockLocation, TEnumAsByte<EBlockType> BlockClass);

	UFUNCTION(BlueprintCallable)
	bool IsPlaceableLocation(FVector Location);

	UFUNCTION(BlueprintPure)
	FVector SnapLocationToGrid(FVector Location);

	UFUNCTION(BlueprintPure)
	void ConvertLocationToChunkAndBlockLocation(FVector Location, FIntPoint& ChunkLocation, FIntVector& BlockLocationInChunk);

	bool CheckCollisionWithBlock(FVector Location);

	UFUNCTION(BlueprintCallable)
	void ResetGeneratedMap();

	UFUNCTION(BlueprintCallable)
	bool GetGeneratedChunk(FIntPoint chunkLocation, FChunkData& chunkData);

	UFUNCTION(BlueprintCallable)
	void AddGeneratedChunk(FIntPoint chunkLocation, FChunkData chunkData);

	UFUNCTION(BlueprintCallable)
	bool SetBlockInChunk(FIntPoint chunkLocation, FIntVector blockLocation, TEnumAsByte<EBlockType> blockType);
	
	UFUNCTION(BlueprintCallable)
	TSubclassOf<class ABlock> GetBlockClassFromType(TEnumAsByte<EBlockType> blockType);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int ChunkDefaultHeight = 16;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int ChunkMaxHeight = 32;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int ChunkSize = 16;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int CurrentSeed = 4;

protected:

	UFUNCTION(BlueprintCallable)
	void SetGeneratedMap(TMap<FIntPoint, FChunkData> generatedMap);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UDataTable* BlockDataTable;

	UPROPERTY(BlueprintReadOnly)
	TMap<FIntPoint, FChunkData> GeneratedMap;
};
