// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

	InventorySlots.Init(FInventorySlot(), MaxInventorySize);
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


FInventorySlot* UInventoryComponent::FindItemInInventory(TSubclassOf<class AItem> Item)
{
	if (!Item)
		return nullptr;

	for (FInventorySlot& slot : InventorySlots)
	{
		if (slot.Item == Item) {
			return  &slot;
		}
	}

	return nullptr;
}

FInventorySlot* UInventoryComponent::FindFirstEmptyInventorySlot()
{
	for (FInventorySlot& slot : InventorySlots)
	{
		if (!slot.Item) {
			return &slot;
		}
	}

	return nullptr;
}

// Called when the game starts
int UInventoryComponent::StoreItems(TSubclassOf<class AItem> Item, int Count)
{
	if (!Item)
		return 0;

	if (Count <= 0)
		return 0;
	
	FInventorySlot* slotPtr = FindItemInInventory(Item);
		
	if (slotPtr)
	{
		int storedItems = Count;

		slotPtr->ItemCount += Count;

		OnInventoryChanged.Broadcast();
		return storedItems;
	}

	slotPtr = FindFirstEmptyInventorySlot();

	if (slotPtr)
	{
		int storedItems = Count;

		slotPtr->Item = Item;
		slotPtr->ItemCount += Count;

		OnInventoryChanged.Broadcast();
		return storedItems;
	}

	return 0;
}


// Called when the game starts
int UInventoryComponent::ConsumeItems(TSubclassOf<class AItem> Item, int Count)
{
	if (!Item)
		return 0;

	if (Count <= 0)
		return 0;

	FInventorySlot* slotPtr = FindItemInInventory(Item);

	if (slotPtr)
	{
		int consumedItems = Count;
		
		if (slotPtr->ItemCount <= Count)
		{
			slotPtr->Item = nullptr;

			consumedItems = slotPtr->ItemCount;

			slotPtr->ItemCount = 0;
		}
		else {
			slotPtr->ItemCount -= Count;
		}

		OnInventoryChanged.Broadcast();
		return consumedItems;
	}

	return 0;
}


int UInventoryComponent::GetItemCount(TSubclassOf<class AItem> Item)
{
	if (!Item)
		return 0;

	FInventorySlot* slotPtr = FindItemInInventory(Item);

	if (slotPtr)
	{
		return slotPtr->ItemCount;
	}

	return 0;
}


bool UInventoryComponent::SetSelectedInventorySlot(int newIndex)
{
	if (!InventorySlots.IsValidIndex(newIndex))
		return false;

	SelectedInventorySlotIndex = newIndex;

	OnSelectedInventorySlot.Broadcast(InventorySlots[SelectedInventorySlotIndex]);

	return true;
}


bool UInventoryComponent::GetSelectedInventorySlot(FInventorySlot& SelectedSlot)
{
	if (!InventorySlots.IsValidIndex(SelectedInventorySlotIndex))
		return false;

	SelectedSlot = InventorySlots[SelectedInventorySlotIndex];
	return true;
}