// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "../Entities/Item.h"
#include "Components/ActorComponent.h"
#include "InventoryComponent.generated.h"


USTRUCT(BlueprintType)
struct FInventorySlot 
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default")
	TSubclassOf<class AItem> Item = nullptr;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default")
	int ItemCount = 0;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInventoryChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSelectedInventorySlot, FInventorySlot, InventorySlot);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent, IsBlueprintBase="true") )
class GRIPCRAFT_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()
		
public:	
	// Sets default values for this component's properties
	UInventoryComponent();
	

	UPROPERTY(BlueprintAssignable)
	FInventoryChanged OnInventoryChanged;

	UPROPERTY(BlueprintAssignable)
	FSelectedInventorySlot OnSelectedInventorySlot;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	TArray<FInventorySlot> InventorySlots;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int MaxInventorySize = 5;

	UPROPERTY(BlueprintReadOnly)
	int SelectedInventorySlotIndex = -1;

	FInventorySlot* FindItemInInventory(TSubclassOf<class AItem> Item);

	FInventorySlot* FindFirstEmptyInventorySlot();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	TArray<FInventorySlot> GetInventorySlots() const { return InventorySlots; }

	UFUNCTION(BlueprintCallable)
	bool SetSelectedInventorySlot(int newIndex);

	UFUNCTION(BlueprintCallable)
	int StoreItems(TSubclassOf<class AItem> Item, int Count);

	UFUNCTION(BlueprintCallable)
	int ConsumeItems(TSubclassOf<class AItem> Item, int Count);

	UFUNCTION(BlueprintCallable)
	int GetItemCount(TSubclassOf<class AItem> Item);

	UFUNCTION(BlueprintCallable)
	bool GetSelectedInventorySlot(FInventorySlot& SelectedSlot);
};
