// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GripCraftPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GRIPCRAFT_API AGripCraftPlayerController : public APlayerController
{
	GENERATED_BODY()
	
};
