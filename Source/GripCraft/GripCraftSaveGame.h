// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "GripCraftGameInstance.h"

#include "GripCraftSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class GRIPCRAFT_API UGripCraftSaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	void SetSaveData(int seed, TMap<FIntPoint, FChunkData> mapData);
	
protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Default")
	int Seed;

	UPROPERTY(BlueprintReadOnly)
	TMap<FIntPoint, FChunkData> GeneratedMap;



};
