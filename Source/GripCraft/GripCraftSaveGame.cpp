// Fill out your copyright notice in the Description page of Project Settings.


#include "GripCraftSaveGame.h"

void UGripCraftSaveGame::SetSaveData(int seed, TMap<FIntPoint, FChunkData> mapData)
{
	Seed = seed;

	GeneratedMap = mapData;
}
