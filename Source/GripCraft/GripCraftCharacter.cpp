// Copyright Epic Games, Inc. All Rights Reserved.

#include "GripCraftCharacter.h"
#include "GripCraftProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AGripCraftCharacter

AGripCraftCharacter::AGripCraftCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
	RootComponent = GetCapsuleComponent();

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));	

	MeshInHand = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshInHand"));	


	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->InitBoxExtent(FVector(45.0f, 45.0f, 90.0f));
	BoxComponent->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	BoxComponent->SetupAttachment(GetCapsuleComponent());

	BoxComponent->CanCharacterStepUpOn = ECB_No;
	BoxComponent->SetShouldUpdatePhysicsVolume(true);
	BoxComponent->SetCanEverAffectNavigation(false);
	BoxComponent->bDynamicObstacle = true;


}

void AGripCraftCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	if (MeshInHand)
		MeshInHand->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
}

//////////////////////////////////////////////////////////////////////////
// Input

void AGripCraftCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind action event
	PlayerInputComponent->BindAction("Action", IE_Pressed, this, &AGripCraftCharacter::OnAction);
	PlayerInputComponent->BindAction("Action", IE_Released, this, &AGripCraftCharacter::OnStopAction);

	// Bind use item event
	PlayerInputComponent->BindAction("UseItem", IE_Pressed, this, &AGripCraftCharacter::OnUseItem);
	PlayerInputComponent->BindAction("UseItem", IE_Released, this, &AGripCraftCharacter::OnStopUseItem);
		
	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AGripCraftCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGripCraftCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AGripCraftCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AGripCraftCharacter::LookUpAtRate);
}

void AGripCraftCharacter::OnAction()
{
	if (ItemInHand) {
		ItemInHand->StartActionItem(this);
	}
}

void AGripCraftCharacter::OnStopAction()
{
	if (ItemInHand) {
		ItemInHand->StopActionItem(this);
	}
}

void AGripCraftCharacter::OnUseItem()
{
	if (ItemInHand) {
		ItemInHand->StartUseItem(this);
	}
}

void AGripCraftCharacter::OnStopUseItem()
{
	if (ItemInHand) {
		ItemInHand->StopUseItem(this);
	}
}

void AGripCraftCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AGripCraftCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AGripCraftCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AGripCraftCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AGripCraftCharacter::GetCameraHitResult(float maxDistance, ECollisionChannel collisionChannel, FHitResult& outHit)
{
	UWorld* world = GetWorld();

	if (!world)
		return false;

	if (!FirstPersonCameraComponent)
		return false;

	const FVector traceStart = FirstPersonCameraComponent->GetComponentLocation();
	const FVector traceEnd = traceStart + FirstPersonCameraComponent->GetForwardVector() * maxDistance;

	FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
	traceParams.bTraceComplex = false;
	traceParams.bReturnPhysicalMaterial = false;	

	FCollisionObjectQueryParams params = FCollisionObjectQueryParams(FCollisionObjectQueryParams::AllDynamicObjects);
	
	return world->LineTraceSingleByChannel(
		outHit,
		traceStart,
		traceEnd,
		collisionChannel,
		traceParams
	);
}

bool AGripCraftCharacter::GetCameraHitResults(float maxDistance, ECollisionChannel collisionChannel, TArray<FHitResult>& outHits)
{
	
	UWorld* world = GetWorld();

	if (!world)
		return false;

	if (!FirstPersonCameraComponent)
		return false;

	const FVector traceStart = FirstPersonCameraComponent->GetComponentLocation();
	const FVector traceEnd = traceStart + FirstPersonCameraComponent->GetForwardVector() * maxDistance;

	FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
	traceParams.bTraceComplex = false;
	traceParams.bReturnPhysicalMaterial = false;


	return world->LineTraceMultiByChannel(
		outHits,
		traceStart,
		traceEnd,
		collisionChannel,
		traceParams
	);
}

void AGripCraftCharacter::SetItemInHand(class AItem* newItem)
{
	if (ItemInHand) {
		ItemInHand->ItemGotOutHand(this);
	}

	ItemInHand = newItem;

	if (ItemInHand) {
		ItemInHand->ItemGotInHand(this);
	}
}