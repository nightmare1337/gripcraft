// Fill out your copyright notice in the Description page of Project Settings.


#include "GripCraftGameInstance.h"

void UGripCraftGameInstance::ResetGeneratedMap()
{
	GeneratedMap.Empty();
}

void UGripCraftGameInstance::SetGeneratedMap(TMap<FIntPoint, FChunkData> generatedMap)
{
	GeneratedMap = generatedMap;
}

bool UGripCraftGameInstance::GetGeneratedChunk(FIntPoint chunkLocation, FChunkData& chunkData)
{
	if (GeneratedMap.Contains(chunkLocation)) {
		chunkData = GeneratedMap[chunkLocation];
		return true;
	}

	return false;
}

void UGripCraftGameInstance::AddGeneratedChunk(FIntPoint chunkLocation, FChunkData chunkData)
{
	GeneratedMap.Add(chunkLocation, chunkData);
}

bool UGripCraftGameInstance::SetBlockInChunk(FIntPoint chunkLocation, FIntVector blockLocation, TEnumAsByte<EBlockType> blockType)
{
	if (GeneratedMap.Contains(chunkLocation)) {		
		FChunkData chunkData = GeneratedMap[chunkLocation];			

		int index = chunkData.GetLocationIndex(chunkData.Size, blockLocation);

		if (!chunkData.Blocks.IsValidIndex(index))
			return false;

		chunkData.Blocks[index] = blockType;

		AddGeneratedChunk(chunkLocation, chunkData);
		return true;
	}

	return false;
}

TSubclassOf<ABlock> UGripCraftGameInstance::GetBlockClassFromType(TEnumAsByte<EBlockType> blockType)
{
	FString ContextString;
	TArray<FBlockTypeClassRow*> outArray;
	TArray<FName> RowNames = BlockDataTable->GetRowNames();

	for (auto& name : BlockDataTable->GetRowMap())
	{
		FBlockTypeClassRow* row = (FBlockTypeClassRow*)(name.Value);
		if (row && row->BlockType == blockType)
		{
			return row->BlockClass;
		}
	}

	return nullptr;
}


ABlock* UGripCraftGameInstance::PlaceBlock(FVector Location, TEnumAsByte<EBlockType> BlockType)
{
	if (BlockType == EBlockType::Air)
		return nullptr;

	UWorld* world = GetWorld();

	if (!world)
		return nullptr;

	FVector snappedLocation = SnapLocationToGrid(Location);

	if (IsPlaceableLocation(snappedLocation))
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		TSubclassOf<ABlock> blockClass = GetBlockClassFromType(BlockType);

		if (!blockClass)
			return nullptr;

		ABlock* newBlock = world->SpawnActor<ABlock>(blockClass, snappedLocation, FRotator::ZeroRotator, SpawnInfo);

		FIntPoint chunkCoords;
		FIntVector blockCoords;

		ConvertLocationToChunkAndBlockLocation(snappedLocation, chunkCoords, blockCoords);

		newBlock->ChunkCoords = chunkCoords;
		newBlock->BlockCoordsInChunk = blockCoords;

		SetBlockInChunk(chunkCoords, blockCoords, BlockType);

		return newBlock;
	}

	return nullptr;
};


ABlock* UGripCraftGameInstance::SpawnBlockGenerated(FIntPoint ChunkLocation, FIntVector BlockLocation, TEnumAsByte<EBlockType> BlockType)
{
	if (BlockType == EBlockType::Air)
		return nullptr;

	UWorld* world = GetWorld();

	if (!world)
		return nullptr;

	FVector snappedLocation = SnapLocationToGrid(
		FVector((ChunkLocation.X * 16 + BlockLocation.X) * 100, (ChunkLocation.Y * 16 + BlockLocation.Y) * 100, BlockLocation.Z * 100));

	if (CheckCollisionWithBlock(snappedLocation)) {
		return nullptr;
	}

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	TSubclassOf<ABlock> blockClass = GetBlockClassFromType(BlockType);

	if (!blockClass)
		return nullptr;

	ABlock* newBlock = world->SpawnActor<ABlock>(blockClass, snappedLocation, FRotator::ZeroRotator, SpawnInfo);

	newBlock->ChunkCoords = ChunkLocation;
	newBlock->BlockCoordsInChunk = BlockLocation;

	return newBlock;
};

bool UGripCraftGameInstance::CheckCollisionWithBlock(FVector Location)
{
	UWorld* world = GetWorld();

	if (!world)
		return false;

	const TArray <TEnumAsByte<EObjectTypeQuery>> &ObjectTypes = TArray <TEnumAsByte<EObjectTypeQuery>>();
	UClass *ActorClassFilter = ABlock::StaticClass();
	const TArray < AActor*> &ActorsToIgnore = TArray< AActor*>();
	TArray <AActor*> OutActors;

	bool result = UKismetSystemLibrary::SphereOverlapActors(world, Location, 1.0f, ObjectTypes, ActorClassFilter, ActorsToIgnore, OutActors);

	if (!result)
		return false;

	return OutActors.Num() > 0;
}

bool UGripCraftGameInstance::IsPlaceableLocation(FVector Location)
{
	// Check if collides with any other block
	if (CheckCollisionWithBlock(Location)) {
		return false;
	}

	// Check if there is at least one adjecent block
	if (CheckCollisionWithBlock(Location + (FVector::DownVector * 75)))
		return true;

	if (CheckCollisionWithBlock(Location + (FVector::UpVector * 75)))
		return true;

	if (CheckCollisionWithBlock(Location + (FVector::LeftVector * 75)))
		return true;

	if (CheckCollisionWithBlock(Location + (FVector::RightVector * 75)))
		return true;

	if (CheckCollisionWithBlock(Location + (FVector::ForwardVector * 75)))
		return true;

	if (CheckCollisionWithBlock(Location + (FVector::BackwardVector * 75)))
		return true;

	return false;
}

FVector UGripCraftGameInstance::SnapLocationToGrid(FVector Location)
{
	return UKismetMathLibrary::Vector_SnappedToGrid(Location, 100);
}


void UGripCraftGameInstance::ConvertLocationToChunkAndBlockLocation(FVector Location, FIntPoint& ChunkLocation, FIntVector& BlockLocationInChunk)
{
	int chunkX = FMath::RoundToInt(Location.X / ChunkSize / 100);
	int chunkY = FMath::RoundToInt(Location.Y / ChunkSize / 100);

	int blockX = (Location.X / 100) - (chunkX * ChunkSize);
	int blockY = (Location.Y / 100) - (chunkY * ChunkSize);
	int blockZ = (Location.Z / 100);

	ChunkLocation = FIntPoint(chunkX, chunkY);
	BlockLocationInChunk = FIntVector(blockX, blockY, blockZ);
}