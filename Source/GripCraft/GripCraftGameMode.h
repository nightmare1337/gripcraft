// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GripCraftGameMode.generated.h"

UCLASS(minimalapi)
class AGripCraftGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGripCraftGameMode();
};



