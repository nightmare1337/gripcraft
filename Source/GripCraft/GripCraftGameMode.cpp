// Copyright Epic Games, Inc. All Rights Reserved.

#include "GripCraftGameMode.h"
#include "GripCraftHUD.h"
#include "GripCraftCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGripCraftGameMode::AGripCraftGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AGripCraftHUD::StaticClass();
}
