// Fill out your copyright notice in the Description page of Project Settings.


#include "Generator.h"

// Sets default values
AGenerator::AGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGenerator::BeginPlay()
{
	Super::BeginPlay();

	ResetMap();
}

// Called every frame
void AGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGenerator::ResetMap()
{
	SpawnedChunks.Empty();
}

FChunkData AGenerator::GenerateChunk_Internal(float scale, int octaves, float persistance, float lacunarity, FIntPoint chunkLocation)
{
	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return FChunkData();

	int chunkSize = gameInstance->ChunkSize;

	float** generatedChunkHeight = new float*[chunkSize];

	for (int i = 0; i < chunkSize; i++) {
		generatedChunkHeight[i] = new float[chunkSize];
	}

	FRandomStream randomStream = FRandomStream(gameInstance->CurrentSeed);
	UKismetMathLibrary::SetRandomStreamSeed(randomStream, gameInstance->CurrentSeed);

	TArray<FVector2D> octaveOffsets = TArray<FVector2D>();

	float maxPossibleHeight = 0;
	float amplitude = 1;
	float frequency = 1;

	for (int i = 0; i < octaves; i++) {
		float offsetX = randomStream.FRandRange(-10000, 10000) + (chunkLocation.X * chunkSize * 100);
		float offsetY = randomStream.FRandRange(-10000, 10000) - (chunkLocation.Y * chunkSize * 100);
		octaveOffsets.Add(FVector2D(offsetX, offsetY));

		maxPossibleHeight += amplitude;
		amplitude *= persistance;
	}

	float halfWidth = chunkSize / 2;
	float halfHeight = chunkSize / 2;
	
	if (scale <= 0)
		scale = 0.001f;

	for (int x = 0; x < chunkSize; x++)
	{
		for (int y = 0; y < chunkSize; y++)
		{
			float noiseHeight = 0;
			amplitude = 1;
			frequency = 1;

			for (int i = 0; i < octaves; i++)
			{
				float sampleX = ((x - halfWidth + octaveOffsets[i].X) / (scale)) * frequency;
				float sampleY = ((y - halfHeight + octaveOffsets[i].Y) / (scale)) * frequency;

				FVector2D location = FVector2D(sampleX, sampleY);

				float perlinValue = FMath::PerlinNoise2D(location);

				noiseHeight += perlinValue * amplitude;

				amplitude *= persistance;
				frequency *= lacunarity;
			}
			
			generatedChunkHeight[x][y] = noiseHeight;
		}
	}
	

	FChunkData chunkData = FChunkData();
	chunkData.Location = chunkLocation;
	chunkData.Size = chunkSize;
	chunkData.Blocks = TArray<TEnumAsByte<EBlockType>>();
	chunkData.Blocks.Init(EBlockType::Air, chunkSize * chunkSize * gameInstance->ChunkMaxHeight);
	chunkData.HeighestBlock = FIntVector(-1, -1, -1);

	int heighestBlock = -1;

	float halfDepth = gameInstance->ChunkDefaultHeight / 2;
	
	for (int x = 0; x < chunkSize; x++)
	{
		for (int y = 0; y < chunkSize; y++)
		{
			/*
			float normalizedHeight = (ChunkData[x][y] + 1) / (2 * maxPossibleHeight / 1.75f);
			ChunkData[x][y] = FMath::Clamp<float>(normalizedHeight, 0, INT_MAX);*/

			generatedChunkHeight[x][y] = (generatedChunkHeight[x][y] + 1) / (2 * maxPossibleHeight);


			int currentCollumnHighestBlock = FMath::RoundToInt((generatedChunkHeight[x][y] * HeightScale) + gameInstance->ChunkDefaultHeight);

			if (currentCollumnHighestBlock > heighestBlock) {
				heighestBlock = currentCollumnHighestBlock;
				chunkData.HeighestBlock = FIntVector(x, y, heighestBlock);
			}

			float sampleX = (x - halfWidth) / scale;
			float sampleY = (y - halfHeight) / scale;

			for (int z = currentCollumnHighestBlock; z >= 0; z--) {
				float sampleZ = (z - halfDepth) / scale;
				
				float perlinValue = FMath::PerlinNoise3D(FVector(sampleX, sampleY, sampleZ));
				
				EBlockType blockType = GetBlockTypeFromPerlin(perlinValue, scale);

				//EBlockType blockType = GetBlockTypeFromHeight(z);

				chunkData.Blocks[chunkData.GetLocationIndex(chunkSize, x,y,z)] = blockType;
			}
		}
	}

	return chunkData;
}

void AGenerator::GenerateChunk(FIntPoint chunkLocation)
{
	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return;

	int seed = gameInstance->CurrentSeed;
	int chunkSize = gameInstance->ChunkSize;

	FChunkData chunkData;

	if (!gameInstance->GetGeneratedChunk(chunkLocation, chunkData)) {
		chunkData = GenerateChunk_Internal(NoiseScale, Octaves, Persistance, Lacunarity, chunkLocation);

		gameInstance->AddGeneratedChunk(chunkLocation, chunkData);
	}
}


void AGenerator::SpawnChunk(FIntPoint chunkLocation)
{
	if (SpawnedChunks.Contains(chunkLocation))
		return;
	
	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return;

	FChunkData chunkData;

	if (gameInstance->GetGeneratedChunk(chunkLocation, chunkData)) {
		SpawnChunk_Internal(chunkData);
	}
	else {
		GenerateChunk(chunkLocation);

		RespawnChunkIfGenerated(chunkLocation + FIntPoint(1, 0));
		RespawnChunkIfGenerated(chunkLocation - FIntPoint(1, 0));
		RespawnChunkIfGenerated(chunkLocation + FIntPoint(0, 1));
		RespawnChunkIfGenerated(chunkLocation - FIntPoint(0, 1));

		if (gameInstance->GetGeneratedChunk(chunkLocation, chunkData)) {
			SpawnChunk_Internal(chunkData);
		}
	}
}

void AGenerator::RespawnChunkIfGenerated(FIntPoint chunkLocation)
{
	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return;

	FChunkData chunkData;

	if (gameInstance->GetGeneratedChunk(chunkLocation, chunkData)) {
		SpawnChunk_Internal(chunkData);
	}
}

void AGenerator::SpawnChunk_Internal(FChunkData chunkData)
{
	UWorld* world = GetWorld();

	if (!world)
		return;

	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return;

	for (int x = 0; x < chunkData.Size; x++)
	{
		for (int y = 0; y < chunkData.Size; y++)
		{
			float spawnX = (chunkData.Location.X * gameInstance->ChunkSize * 100) + (x * 100);
			float spawnY = (chunkData.Location.Y * gameInstance->ChunkSize * 100) + (y * 100);
			
			for (int z = chunkData.HeighestBlock.Z; z >= 0; z--) {
				EBlockType blockType = chunkData.Blocks[chunkData.GetLocationIndex(chunkData.Size, x, y, z)];

				if (blockType == EBlockType::Air)
					continue;

				FIntVector blockLocation = FIntVector(x, y, z);

				if (IsBlockAdjacentToAir(blockLocation, chunkData.Location)) {
					FVector spawnLocation = FVector(spawnX, spawnY, z * 100);
					
					ABlock* spawnedBlock = gameInstance->SpawnBlockGenerated(chunkData.Location, blockLocation, blockType);

					if (spawnedBlock)
						spawnedBlock->OnBlockDestroyed.AddDynamic(this, &AGenerator::OnBlockDestroyed);
				}
			}
		}
	}

	SpawnedChunks.Add(chunkData.Location);
}


void AGenerator::DespawnChunk(FIntPoint chunkLocation)
{
	if (!SpawnedChunks.Contains(chunkLocation))
		return;

	UWorld* world = GetWorld();

	if (!world)
		return;

	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return;

	int chunkSize = gameInstance->ChunkSize;

	float posX = chunkLocation.X * chunkSize * 100 + chunkSize / 2 * 100 - 50;
	float posY = chunkLocation.Y * chunkSize * 100 + chunkSize / 2 * 100 - 50;
	float posZ = gameInstance->ChunkMaxHeight / 2 * 100;
	
	FVector BoxPos = FVector(posX, posY, posZ);
	FVector BoxExtent = FVector(chunkSize /2*100 - 25, chunkSize / 2 * 100 - 25, gameInstance->ChunkMaxHeight / 2 * 100 - 25);

	const TArray <TEnumAsByte<EObjectTypeQuery>> &ObjectTypes = TArray <TEnumAsByte<EObjectTypeQuery>>();
	UClass *ActorClassFilter = ABlock::StaticClass();
	const TArray < AActor*> &ActorsToIgnore = TArray< AActor*>();
	TArray <class AActor*> OutActors;

	if (UKismetSystemLibrary::BoxOverlapActors(world, BoxPos, BoxExtent, ObjectTypes, ActorClassFilter, ActorsToIgnore, OutActors))
	{
		for (AActor* blockToDespawn : OutActors) {
			if (!blockToDespawn)
				continue;

			blockToDespawn->Destroy();
		}
	}

	if (SpawnedChunks.Remove(chunkLocation))
		return;
}

bool AGenerator::IsBlockAdjacentToAir(FIntVector BlockLocation, FIntPoint ChunkLocation)
{
	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return false;

	if (BlockLocation.X < 15)
	{
		if (IsBlockAir(BlockLocation + FIntVector(1, 0, 0), ChunkLocation))
		{
			return true;
		}
	}		
	else {
		if (IsBlockAir(FIntVector(0, BlockLocation.Y, BlockLocation.Z), ChunkLocation + FIntPoint(1, 0)))
		{
			return true;
		}
	}
		

	if (BlockLocation.X > 0)
	{
		if (IsBlockAir(BlockLocation - FIntVector(1, 0, 0), ChunkLocation))
		{
			return true;
		}
	}		
	else {
		if (IsBlockAir(FIntVector(15, BlockLocation.Y, BlockLocation.Z), ChunkLocation - FIntPoint(1, 0)))
		{
			return true;
		}
	}
	
	if (BlockLocation.Y < 15)
	{
		if (IsBlockAir(BlockLocation + FIntVector(0, 1, 0), ChunkLocation))
		{
			return true;
		}
	}	
	else
	{
		if (IsBlockAir(FIntVector(BlockLocation.X, 0, BlockLocation.Z), ChunkLocation + FIntPoint(0, 1)))		
		{
			return true;
		}
	}
		

	if (BlockLocation.Y > 0)
	{
		if (IsBlockAir(BlockLocation - FIntVector(1, 0, 0), ChunkLocation))
		{
			return true;
		}
	}		
	else
	{
		if (IsBlockAir(FIntVector(BlockLocation.X, 15, BlockLocation.Z), ChunkLocation - FIntPoint(0, 1)))
		{
			return true;
		}
	}
		

	if (BlockLocation.Z < gameInstance->ChunkMaxHeight)
	{
		if (IsBlockAir(BlockLocation + FIntVector(0, 0, 1), ChunkLocation))
		{
			return true;
		}			
	}
		

	if (BlockLocation.Z > 0)
	{
		if (IsBlockAir(BlockLocation - FIntVector(0, 0, 1), ChunkLocation)) 
		{
			return true;
		}
	}
		

	return false;
}


bool AGenerator::IsBlockAir(FIntVector blockLocation, FIntPoint chunkLocation)
{
	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return false;
	
	FChunkData chunkData;

	if (gameInstance->GetGeneratedChunk(chunkLocation, chunkData)) {
		
		int index = chunkData.GetLocationIndex(chunkData.Size, blockLocation);

		if (!chunkData.Blocks.IsValidIndex(index))
			return false;

		return chunkData.Blocks[index] == EBlockType::Air;
	}

	return false;
}

TEnumAsByte<EBlockType> AGenerator::GetBlockTypeFromPerlin(float perlinValue, float scale)
{
	if (perlinValue < -.4f / scale)
	{
		return EBlockType::Stone;
	}
	else if (perlinValue < 0.0f / scale)
	{
		return EBlockType::Gravel;
	}
	else if (perlinValue < .6f / scale)
	{
		return EBlockType::Grass;
	}
	else if (perlinValue < .9f / scale)
	{
		return EBlockType::Wood;
	}
	else
	{
		return EBlockType::Grass;
	}
}

TEnumAsByte<EBlockType> AGenerator::GetBlockTypeFromHeight(int Height)
{
	if (Height <= 5) {
		return EBlockType::Stone;
	}
	else if (Height < 7)
	{
		return EBlockType::Gravel;
	}
	else if (Height < 15)
	{
		return EBlockType::Grass;
	}
	else if (Height < 17)
	{
		return EBlockType::Wood;
	}
	else
	{
		return EBlockType::Grass;
	}
}

void AGenerator::OnBlockDestroyed(FIntVector BlockLocation, FIntPoint ChunkLocation)
{
	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return;

	gameInstance->SetBlockInChunk(ChunkLocation, BlockLocation, EBlockType::Air);
	
	if (BlockLocation.Z < gameInstance->ChunkMaxHeight)
		CheckBlockIfVisibleAfterBlockDestroyed(BlockLocation + FIntVector(0, 0, 1), ChunkLocation);

	if (BlockLocation.Z > 0)
		CheckBlockIfVisibleAfterBlockDestroyed(BlockLocation - FIntVector(0, 0, 1), ChunkLocation);

	if (BlockLocation.X < 15)
		CheckBlockIfVisibleAfterBlockDestroyed(BlockLocation + FIntVector(1, 0, 0), ChunkLocation);
	else
		CheckBlockIfVisibleAfterBlockDestroyed(FIntVector(0, BlockLocation.Y, BlockLocation.Z), ChunkLocation + FIntPoint(1, 0));

	if (BlockLocation.X > 0)
		CheckBlockIfVisibleAfterBlockDestroyed(BlockLocation - FIntVector(1, 0, 0), ChunkLocation);
	else
		CheckBlockIfVisibleAfterBlockDestroyed(FIntVector(15, BlockLocation.Y, BlockLocation.Z), ChunkLocation - FIntPoint(1, 0));


	if (BlockLocation.Y < 15)
		CheckBlockIfVisibleAfterBlockDestroyed(BlockLocation + FIntVector(0, 1, 0), ChunkLocation);
	else
		CheckBlockIfVisibleAfterBlockDestroyed(FIntVector(BlockLocation.X, 0, BlockLocation.Z), ChunkLocation + FIntPoint(0, 1));
	
	if (BlockLocation.Y > 0)
		CheckBlockIfVisibleAfterBlockDestroyed(BlockLocation - FIntVector(0, 1, 0), ChunkLocation);
	else
		CheckBlockIfVisibleAfterBlockDestroyed(FIntVector(BlockLocation.X, 15, BlockLocation.Z), ChunkLocation - FIntPoint(0, 1));

}

void AGenerator::CheckBlockIfVisibleAfterBlockDestroyed(FIntVector blockLocation, FIntPoint chunkLocation)
{
	UWorld* world = GetWorld();

	UGripCraftGameInstance* gameInstance = GetGameInstance<UGripCraftGameInstance>();

	if (!gameInstance)
		return;

	FChunkData chunkData;

	if (gameInstance->GetGeneratedChunk(chunkLocation, chunkData)) {
		
		EBlockType blockType = chunkData.Blocks[chunkData.GetLocationIndex(chunkData.Size, blockLocation)];

		if (blockType == EBlockType::Air)
			return;
		
		ABlock* spawnedBlock = gameInstance->SpawnBlockGenerated(chunkLocation, blockLocation, blockType);

		if (spawnedBlock) {
			spawnedBlock->OnBlockDestroyed.AddDynamic(this, &AGenerator::OnBlockDestroyed);
		}
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Failed to find Chunk: %f : %f"), chunkLocation.X, chunkLocation.Y);
	}
}