// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Kismet/KismetMathLibrary.h"
#include "../Entities/Block.h"
#include "../GripCraftFunctions.h"
#include "../GripCraftGameInstance.h"

#include "Engine/DataTable.h"

#include "Generator.generated.h"

UCLASS(meta = (IsBlueprintBase = "true"))
class GRIPCRAFT_API AGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGenerator();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float NoiseScale = 0.01f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HeightScale = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Octaves = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Persistance = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Lacunarity = 1.0f;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FIntPoint> SpawnedChunks;


	UFUNCTION(BlueprintCallable)
	void ResetMap();

	UFUNCTION(BlueprintCallable)
	void GenerateChunk(FIntPoint chunkLocation);

	UFUNCTION(BlueprintCallable)
	void SpawnChunk(FIntPoint chunkLocation);
	
	UFUNCTION(BlueprintCallable)
	void DespawnChunk(FIntPoint chunkLocation);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:


	FChunkData GenerateChunk_Internal(float scale, int octaves, float persistance, float lacunarity, FIntPoint chunkLocation);
	void SpawnChunk_Internal(FChunkData chunkData);
	
	TEnumAsByte<EBlockType> GetBlockTypeFromPerlin(float perlinValue, float scale);
	TEnumAsByte<EBlockType> GetBlockTypeFromHeight(int height);
	bool IsBlockAdjacentToAir(FIntVector BlockLocation, FIntPoint ChunkLocation);
	bool IsBlockAir(FIntVector BlockLocation, FIntPoint ChunkLocation);
	void RespawnChunkIfGenerated(FIntPoint chunkLocation);

	void CheckBlockIfVisibleAfterBlockDestroyed(FIntVector BlockLocation, FIntPoint ChunkLocation);
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnBlockDestroyed(FIntVector bLocation, FIntPoint cLocation);
};
