// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Block.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBlockDestroyed, FIntVector, bLocation, FIntPoint, cLocation);

UCLASS()
class GRIPCRAFT_API ABlock : public AActor
{
	GENERATED_BODY()



public:	
	// Sets default values for this actor's properties
	ABlock();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* BlockMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class AItem> ItemClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float Hardness;

	UPROPERTY(BlueprintAssignable)
	FBlockDestroyed OnBlockDestroyed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FIntPoint ChunkCoords;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FIntVector BlockCoordsInChunk;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetBlockLocation(FVector Location);

	UFUNCTION(BlueprintCallable)
	void DestroyBlock();


	UFUNCTION(BlueprintCallable)
	static void GetChunkAndBlockLocation(FVector Location, FVector2D& ChunkLocation, FIntVector& BlockLocationInChunk);
};
