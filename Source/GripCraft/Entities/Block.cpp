// Fill out your copyright notice in the Description page of Project Settings.


#include "Block.h"

// Sets default values
ABlock::ABlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh"));
	
	RootComponent = BlockMesh;
}

// Called when the game starts or when spawned
void ABlock::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlock::SetBlockLocation(FVector Location)
{
	int locX = FGenericPlatformMath::TruncToInt(Location.X);
	int locY = FGenericPlatformMath::TruncToInt(Location.Y);
	int locZ = FGenericPlatformMath::TruncToInt(Location.Z);

	float locationX = locX - (locX % 100);
	float locationY = locY - (locY % 100);
	float locationZ = locZ - (locZ % 100);

	FVector desiredLocation = FVector(locationX, locationY, locationZ);

	this->SetActorLocation(FVector(locationX, locationY, locationZ));
}


void ABlock::DestroyBlock()
{
	SetActorEnableCollision(false);

	OnBlockDestroyed.Broadcast(BlockCoordsInChunk, ChunkCoords);
	
	Destroy();
}

void ABlock::GetChunkAndBlockLocation(FVector Location, FVector2D& ChunkLocation, FIntVector& BlockLocationInChunk)
{
	int chunkX = FMath::RoundToInt(Location.X / 16 / 100);
	int chunkY = FMath::RoundToInt(Location.Y / 16 / 100);

	int blockX = (Location.X / 100) - (chunkX * 16);
	int blockY = (Location.Y / 100) - (chunkY * 16);
	int blockZ = (Location.Z / 100);

	ChunkLocation = FVector2D(chunkX, chunkY);
	BlockLocationInChunk = FIntVector(blockX, blockY, blockZ);
}