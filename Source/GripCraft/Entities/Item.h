// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "../GripCraftGameInstance.h"

#include "Item.generated.h"

/**
 * 
 */
UCLASS()
class GRIPCRAFT_API AItem : public AActor
{
	GENERATED_BODY()
	
public:
	AItem();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default")
	class UStaticMesh* ItemMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Default")
	class UTexture2D* ItemVisual;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Default")
	TEnumAsByte<EBlockType>  BlockType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ItemGotInHand(class AGripCraftCharacter* UsingCharacter);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ItemGotOutHand(class AGripCraftCharacter* UsingCharacter);

	// Item Action
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StartActionItem(class AGripCraftCharacter* UsingCharacter);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StopActionItem(class AGripCraftCharacter* UsingCharacter);

	// Item Usage
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StartUseItem(class AGripCraftCharacter* UsingCharacter);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StopUseItem(class AGripCraftCharacter* UsingCharacter);
};
